// ==UserScript==
// @name         Sankaku Complex | No blurred images
// @namespace    none
// @version      0.201703162053
// @description  Will simply remove the blurred images caused by the image having blacklisted tags on the gallary sites of Sankaku Complex
// @author       mysteriousLynx
// @include      *://idol.sankakucomplex.com/*
// @include      *://chan.sankakucomplex.com/*
// @grant        GM_addStyle
// @run-at       document-start
// ==/UserScript==

GM_addStyle(".thumb.blacklisted { display: none !important }");